'''
Created on 28 ago. 2018

@author: Roberto Parra
'''

import os
import jinja2
import webapp2
from database import Item
from google.appengine.ext import ndb


#esta es una variable de entorno para hacer referencia a los templates
JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), extensions=['jinja2.ext.autoescape'], autoescape=True)
'''esta clase maneja la parte principal de la aplicacion web
    nota personal esto se maneja como los antiguos servlets de java'''
class MainHandler(webapp2.RequestHandler):
   
    
    def get(self): 
        template_data=self.getTemplate()
        template = JINJA_ENVIRONMENT.get_template('producto.html')    
        self.response.write(template.render(template_data))
        
    def  post(self):
        
        name=self.request.get('name')
        print 'para cargarse el elemento'
        query=Item.query(Item.name == name )
        item=query.get()
        if item <> None:
            item.key.delete()
        template_data=self.getTemplate()
        template = JINJA_ENVIRONMENT.get_template('producto.html')    
        self.response.write(template.render(template_data))
        #self.redirect('/producto.html')
    
    def getTemplate(self):
        productsList=[]
        for p in Item.query().iter():
                    productsList.append({"id": p.key.id(),
                                         "name": p.name,
                                         "label": p.label,
                                         "description": p.description
                                         
                                        })
        template_data = {
                    "products": productsList,
                } 
        return template_data   

class CatalogoHandler(webapp2.RequestHandler):
    
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('adminProducto.html')
        self.response.write(template.render())  

    def post(self):
        
        producto= Item (name=self.request.get('name'),
                       label = self.request.get('label'),
                       description = self.request.get('description'))
        producto.put()
        template_data=self.getTemplate()
        template = JINJA_ENVIRONMENT.get_template('producto.html')    
        self.response.write(template.render(template_data))
        self.redirect('/producto.html')
    
    def getTemplate(self):
        productsList=[]
        for p in Item.query().iter():
                    productsList.append({"id": p.key.id(),
                                         "name": p.name,
                                         "label": p.label,
                                         "description": p.description
                                         
                                        })
        template_data = {
                    "products": productsList,
                } 
        return template_data 
    
    
    
class SearchHandler(webapp2.RequestHandler):
    
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render())  

    def post(self):
        
        parameter= self.request.get('parameter')
        productsList=[]
        print 'busqueda'
        query= Item.query(ndb.OR(Item.name==parameter, Item.label==parameter))#ndb.gql("Select * FROM Item WHERE name = :1")
        #result=query.bind(parameter)
        for p in query.iter():
            print 'Ingresa en For'
            productsList.append({"id": p.key.id(),
                                         "name": p.name,
                                         "label": p.label,
                                         "description": p.description
                                         
                                        })
         
        template_data = {
                    "products": productsList,
                }
        print productsList
        template = JINJA_ENVIRONMENT.get_template('index.html') 
        self.response.write(template.render(template_data))
      
app = webapp2.WSGIApplication([
    ('/producto.html', MainHandler),
    ('/adminProducto.html', CatalogoHandler),
    ('/', SearchHandler),
], debug=True)
