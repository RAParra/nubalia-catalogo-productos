'''
Created on 28 ago. 2018

@author: Roberto Parra
'''
from google.appengine.ext import ndb
from protorpc import messages
from google.appengine.ext.ndb import msgprop



class Item(ndb.Model):
    name=ndb.StringProperty(required=True)
    label=ndb.StringProperty(required=True)
    description = ndb.StringProperty(required=True)



